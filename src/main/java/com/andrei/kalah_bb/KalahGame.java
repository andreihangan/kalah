/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andrei.kalah_bb;

import java.util.Random;

/**
 *
 * @author Andrei
 */
public class KalahGame {
   public Player playerA, playerB;
   public String nextPlayerMove;
   public boolean gameOver = false;
   public String winner="";

   public KalahGame(){
          }
   public KalahGame(int seeds){
       playerA = new Player(seeds);
       playerB = new Player(seeds);
       Random rand = new Random();
       int r = rand.nextInt(2)+1;
       nextPlayerMove = (r==1)?"A":"B";
   }
 
   public void nextMove(int slot){
        int seeds;
        String localPM=nextPlayerMove;
        if (nextPlayerMove =="A"){
            seeds = playerA.slots[slot];
            playerA.slots[slot]=0;
            nextPlayerMove="B";
        }
        else{
            seeds = playerB.slots[slot];
            playerB.slots[slot]=0;
            nextPlayerMove="A";
        }
        slot++;
       while(seeds>0){
            if (localPM=="A" && seeds>0){
                if(slot<playerA.slots.length){
                    playerA.slots[slot]++;
                    seeds--;
                    slot++;
                    continue;
                }
                if(slot==playerA.slots.length){
                    playerA.score++;
                    seeds--;
                    slot=0;
                    localPM="B";
                    continue;
                }
            }
            if (localPM=="B" && seeds>0){
                if(slot<playerB.slots.length){
                    playerB.slots[slot]++;
                    seeds--;
                    slot++;
                    continue;
                }
                if(slot==playerB.slots.length){
                    playerB.score++;
                    seeds--;
                    slot=0;
                    localPM="A";
                    continue;
                }
            }            
       }
       //Verify if the game is over
       if(playerA.sum()==0){
           playerB.score+=playerB.sum();
           for(int i=0; i<playerB.slots.length; i++){playerB.slots[i]=0;}
           gameOver = true;
           if (playerA.score>playerB.score){winner="A";}
           if (playerA.score==playerB.score){winner="AB";}
           if (playerA.score<playerB.score){winner="B";}
       }
       if(playerB.sum()==0 && playerA.sum()!=0){
           playerA.score+=playerA.sum();
           for(int i=0; i<playerA.slots.length; i++){playerA.slots[i]=0;}
           gameOver = true;
           if (playerA.score>playerB.score){winner="A";}
           if (playerA.score==playerB.score){winner="AB";}
           if (playerA.score<playerB.score){winner="B";}           
       }
   }
}

