/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andrei.kalah_bb;

import java.util.stream.IntStream;

/**
 *
 * @author Andrei
 */
public class Player{
    public int[] slots = new int[6];
    public int score;
    public Player(int seeds){
        for(int i=0; i<slots.length; i++){
            slots[i]=seeds;
        }
        score=0;
    }
    public int sum(){
        int s=0;
        for (int i=0; i<slots.length; i++){s+=slots[i];}
        return s;
    }
}
