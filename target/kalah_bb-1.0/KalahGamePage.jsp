<%-- 
    Document   : Kalah
    Created on : Nov 26, 2016, 11:33:16 PM
    Author     : Andrei
--%>

<%@page import="com.andrei.kalah_bb.KalahGame"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
        <link rel="stylesheet" href="kalah.css">
        <title>Kalah</title>
    </head>
    <body class="w3-light-blue w3-text-white">
        <%
            KalahGame localGame = new KalahGame();
            String disabledA="";
            String disabledB="";
            
            if (request.getSession().getAttribute("game") == null){
                int seeds = Integer.parseInt(request.getParameter("seeds"));
                KalahGame kg = new KalahGame(seeds);
                localGame = kg;
                request.getSession().setAttribute("game", localGame);
                
            }
            else{
                localGame = (KalahGame) request.getSession().getAttribute("game");
            }
            
            if (localGame.nextPlayerMove=="A"){
                disabledB="disabled";
            }
            else{
                disabledA="disabled";
            }
            if(localGame.gameOver){
                disabledA="disabled";
                disabledB="disabled";
            }
        %>
    <center><h1><b>KALAH</b></h1></center><br<br><br>
        <form action="KalahGameServlet">
            <table class="kTable">
                <tr>
                    <td>Score player A: <%=localGame.playerA.score%></td>
                    <%for(int j=5; j>=0; j--){
                        if(localGame.playerA.slots[j]!=0){ %>
                            <td><input class="w3-btn w3-ripple w3-round-xlarge w3-padding-xxlarge w3-deep-purple" type="submit" name=<%="A"+j%> value=<%=localGame.playerA.slots[j]%> <%=disabledA%> ></td>
                        <%}
                        else{%>    
                            <td><input class="w3-btn w3-ripple w3-round-xlarge w3-padding-xxlarge w3-deep-purple" type="submit" name=<%="A"+j%> value=<%=localGame.playerA.slots[j]%> disabled ></td>
                        <%}
                    }%>
                    <td>Player A</td>
                </tr>
                <tr>
                    <td>Player B</td>
                    <%for(int j=0; j<6; j++){
                        if(localGame.playerB.slots[j]!=0){ %>
                            <td><input class="w3-btn w3-ripple w3-round-xlarge w3-padding-xxlarge w3-deep-purple" type="submit" name=<%="A"+j%> value=<%=localGame.playerB.slots[j]%> <%=disabledB%> ></td>
                        <%}
                        else{%>    
                            <td><input class="w3-btn w3-ripple w3-round-xlarge w3-padding-xxlarge w3-deep-purple" type="submit" name=<%="A"+j%> value=<%=localGame.playerB.slots[j]%> disabled ></td>
                        <%}
                    }%>
                    <td>Score player B: <%=localGame.playerB.score%></td>                    
                </tr>  
            </table>
        </form>
    <% if(localGame.gameOver){
        request.getSession().removeAttribute("game");
        String winner="";
        if (localGame.winner=="A"){winner="Game over. The winner is Player A. <br><form action='index.html'><input type='submit' value='Return to start page'></form>";}
        if (localGame.winner=="AB"){winner="Game over. The result is draw. <br><form action='index.html'><input type='submit' value='Return to start page'></form>";}
        if (localGame.winner=="B"){winner="Game over. The winner is Player B. <br><form action='index.html'><input type='submit' value='Return to start page'></form>";}%>
        <center><h1><%=winner%></h1></center>
    <%}
    else{ %>
        <center><h1>Next Player move: <%=localGame.nextPlayerMove%></h1></center>
    <%}%>
    </body>
</html>
    